require 'rails_helper'

RSpec.describe CategoryForm do

  let(:user) { create(:user) }
  let(:form) { CategoryForm.new(category) }

  shared_examples 'category' do
    context 'validate' do
      it 'is expected to return true when ok' do
        params = attributes_for(:category)
        expect(form.validate(params)).to be true
      end

      it 'is expected to return false without name' do
        params = attributes_for(:category, name: '')
        expect(form.validate(params)).to be false
      end

      it 'is expected to return false without color' do
        params = attributes_for(:category, color: '')
        expect(form.validate(params)).to be false
      end

      it 'is expected to return false with unknown color' do
        params = attributes_for(:category, color: 'brown')
        expect(form.validate(params)).to be false
      end
    end

    context 'saving' do
      it 'is expected not to change user by save method' do
        form.validate(attributes_for(:category))
        expect{ form.save }.not_to change(category, :user_id)
      end

      it 'is expected change user by save_with_user' do
        form.validate(attributes_for(:category))
        expect{ form.save_with_user(user) }.to change(category, :user_id)
      end
    end
  end


  context 'when create new category' do
    let(:category) { Category.new }

    it_behaves_like 'category'
  end

  context 'when edit category' do
    let(:category) { create(:category, name: 'Test', color: 'azure', user: create(:user)) }

    it_behaves_like 'category'
  end
end
