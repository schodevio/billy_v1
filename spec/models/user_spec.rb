require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'valid?' do

    it 'expects to have email' do
      # is_expected === expect(subject)
      is_expected.to validate_presence_of :email
    end

    it 'expects to have unique email' do
      is_expected.to validate_uniqueness_of(:email).case_insensitive
    end

    it 'expects to have username' do
      is_expected.to validate_presence_of :username
    end

    it 'expects to have unique username' do
      is_expected.to validate_uniqueness_of(:email).case_insensitive
    end

    it 'expects 6 chars to be min length of password' do
      is_expected.to validate_length_of(:password).is_at_least(6)
    end

    it 'expects 128 chars to be max length of password' do
      is_expected.to validate_length_of(:password).is_at_most(128)
    end

    it 'expects to have currency' do
      is_expected.to validate_presence_of :currency
    end
  end
end
