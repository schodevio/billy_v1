require 'rails_helper'

RSpec.describe Category, type: :model do

  it 'expect to belong to User' do
    is_expected.to belong_to :user
  end

  it 'expects to have name column indexed' do
    is_expected.to have_db_index(:name)
  end

  it 'expects to define color by enum' do
    is_expected.to define_enum_for(:color)
      .with(%w[indigo purple pink red orange yellow green lime teal cyan azure gray gray-dark])
  end
end
