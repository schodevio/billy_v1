FactoryBot.define do
  factory :user do |u|
    u.sequence(:email)     { |t| "user#{t}@example.com" }
    password               '123qwe'
    u.sequence(:username)  { |t| "user#{t}" }
    first_name             'Joe'
    last_name              'Doe'
    currency               'PLN'
  end
end
