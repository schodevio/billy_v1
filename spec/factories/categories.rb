FactoryBot.define do
  factory :category do
    name    'Travels'
    color   'red'
    user
  end
end
