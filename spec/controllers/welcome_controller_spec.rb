require 'rails_helper'

RSpec.describe WelcomeController, type: :controller do

  describe 'routing' do
    it 'expects to route to welcome#home as root' do
      is_expected.to route(:get, '/').to(action: :home)
    end

    it 'expects to route to welcome#dashboard' do
      is_expected.to route(:get, '/dashboard').to(action: :dashboard)
    end
  end

  describe 'GET #home' do
    before(:each) { get :home }

    it 'returns http success' do
      expect(response).to have_http_status(:success)
    end

  end

end
