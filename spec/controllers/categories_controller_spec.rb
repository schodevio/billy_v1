require 'rails_helper'

RSpec.describe CategoriesController, type: :controller do

  let(:user) { create(:user) }

  describe 'GET #index' do
    it 'redirects to sign up (no user)' do
      get :index
      expect(response).to redirect_to(new_user_session_path)
    end

    it 'returns http success' do
      sign_in user

      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET #new' do
    it 'redirects to sign up (no user)' do
      get :new
      expect(response).to redirect_to new_user_session_path
    end

    it 'returns http success' do
      sign_in user

      get :new
      expect(response).to have_http_status(:success)
    end
  end

  describe 'POST #create' do
    it 'redirects to sign up (no user)' do
      post :create, params: { category: attributes_for(:category) }
      expect(response).to redirect_to new_user_session_path
    end

    context 'when user signed in' do
      before(:each) { sign_in user }

      it 'redirects to categories view' do
        post :create, params: { category: attributes_for(:category) }
        expect(response).to redirect_to categories_path
      end

      it 'create new category' do
        expect do
          post :create, params: { category: attributes_for(:category) }
        end.to change { Category.count }.by 1
      end

      it 'create new category for current user' do
        post :create, params: { category: attributes_for(:category) }
        expect(Category.last.user).to eq user
      end

      it 'does not create category without name' do
        expect do
          post :create, params: { category: attributes_for(:category, name: '') }
        end.not_to change(Category.all, :count)
      end

      it 'does not create category without color' do
        expect do
          post :create, params: { category: attributes_for(:category, color: '') }
        end.not_to change(Category.all, :count)
      end

      it 'does not create category with uknown color' do
        expect do
          post :create, params: { category: attributes_for(:category, color: 'brown') }
        end.not_to change(Category.all, :count)
      end
    end
  end

  describe 'GET #edit' do
    let(:category) { create(:category, user: user) }

    it 'redirects to sign up (no user)' do
      get :edit, params: { id: category.id }
      expect(response).to redirect_to new_user_session_path
    end

    it 'returns http success' do
      sign_in user

      get :edit, params: { id: category.id }
      expect(response).to have_http_status(:success)
    end
  end

  describe 'PATCH #update' do
    let(:category) { create(:category, user: user) }

    it 'redirects to sign up (no user)' do
      patch :update, params: { id: category.id,
                               category: { name: 'Sport', color: 'green' } }
      expect(response).to redirect_to new_user_session_path
    end

    context 'when user signed in' do
      before(:each) { sign_in user }

      it 'change category name' do
        expect do
          patch :update, params: { id: category.id, category: { name: 'Sport' } }
        end.to change{ category.reload.name }.to 'Sport'
      end

      it 'change category color' do
        expect do
          patch :update, params: { id: category.id, category: { color: 'green' } }
        end.to change{ category.reload.color }.to 'green'
      end

      it 'does not change category name to empty' do
        expect do
          patch :update, params: { id: category.id, category: { name: '' } }
        end.not_to change(category.reload, :name)
      end

      it 'does not change category color to empty' do
        expect do
          patch :update, params: { id: category.id, category: { color: '' } }
        end.not_to change(category.reload, :color)
      end
    end
  end

  describe 'DELETE #destroy' do
    let!(:category) { create(:category, user: user) }

    it 'redirects to sign up (no user)' do
      delete :destroy, params: { id: category.id }
      expect(response).to redirect_to new_user_session_path
    end

    context 'when user signed in' do
      before(:each) { sign_in user }

      it 'destroy category' do
        expect do
          delete :destroy, params: { id: category.id }
        end.to change { Category.count }.by(-1)
      end
    end
  end
end
