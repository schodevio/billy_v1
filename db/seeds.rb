# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# # ================= User =====================
# User.create(
#   email: 'admin@schodev.io',
#   password: '123qwe',
#   username: 'admin',
#   currency: 'PLN',
#   first_name: 'Joe',
#   last_name: 'Doe'
# )
# puts 'admin@schodev.io has been created'
#
#
# # ================ Categories ===============
#
# pairs = { 'Travels' => 'azure', 'Food' => 'indigo', 'Car' => 'purple' }
#
# pairs.each do |category, color|
#   Category.create(name: category, color: color, user: User.last)
#   puts "Category =#{category}= has beed created."
# end
