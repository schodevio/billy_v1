class CreateCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :categories do |t|
      t.string :name
      t.integer :color, default: 0
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :categories, :name
  end
end
