class CategoryForm < Reform::Form
  model :category

  property :name
  property :color

  validates :name, presence: true
  validates :color, presence: true
  validates :color, inclusion: { in: Category.colors.keys, message: 'is not supported' }

  # Save method when create category
  #
  def save_with_user(user)
    model.user = user
    save
  end
  
end
