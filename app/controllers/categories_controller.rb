class CategoriesController < ApplicationController
  layout 'dashboard'

  # GET /categories
  def index
    @categories = current_user.categories
  end

  def show
  end

  # GET /categories/new
  def new
    @form = CategoryForm.new(Category.new)
  end

  # POST /categories
  def create
    @form = CategoryForm.new(Category.new)

    if @form.validate(params[:category])
      @form.save_with_user(current_user)
      redirect_to categories_path, notice: "Category '#{@form.name}' has been created"
    else
      render :new
    end
  end

  # GET /categories/:id/edit
  def edit
    @form = CategoryForm.new(set_category)
  end

  # PATCH /categories/:id
  def update
    @form = CategoryForm.new(set_category)

    if @form.validate(params[:category])
      @form.save
      redirect_to categories_path, notice: "Category '#{@form.name}' has been updated"
    else
      render :edit
    end
  end

  # DELETE /categories/:id
  def destroy
    # TODO: Service to delete category and move bills to Default Category
    category = set_category
    category.destroy

    redirect_to categories_path, notice: "Category '#{category.name}' has been deleted"
  end

  private

  def set_category
    current_user.categories.find(params[:id])
  end

end
