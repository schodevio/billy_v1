class WelcomeController < ApplicationController
  skip_before_action :authenticate_user!, only: :home

  layout :set_layout

  def home
  end

  def dashboard
  end

  private

  def set_layout
    action_name == 'home' ? 'application' : 'dashboard'
  end
end
