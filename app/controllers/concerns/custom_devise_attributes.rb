module CustomDeviseAttributes
  extend ActiveSupport::Concern

  included do
    before_action :configure_permitted_parameters, if: :devise_controller?
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: custom_attributes)
    devise_parameter_sanitizer.permit(:account_update, keys: custom_attributes)
  end

  def custom_attributes
    %i[username first_name last_name currency]
  end
end
