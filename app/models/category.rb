class Category < ApplicationRecord
  belongs_to :user

  enum color: %w[indigo purple pink red orange yellow green lime teal cyan azure gray gray-dark]

end
