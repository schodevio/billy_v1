class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable, :trackable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :categories

  validates :username, presence: true,
                       uniqueness: { case_sensitive: false }

  validates :currency, presence: true

end
