source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.0'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.0'
# Use sqlite3 as the database for Active Record
gem 'sqlite3'
# Use Puma as the app server
gem 'puma', '~> 3.11'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'mini_racer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

group :development, :test do
  # Brakeman is an open source static analysis tool which checks Ruby on Rails applications for security vulnerabilities.
  gem 'brakeman', '~> 4.2', '>= 4.2.1'

  # Patch level verification for bundler (checking gems)
  gem 'bundler-audit', '~> 0.6.0'

  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]

  # Framework for factories
  gem 'factory_bot_rails', '~> 4.8', '>= 4.8.2'

  # Provide fake data
  gem 'faker', '~> 1.8', '>= 1.8.7'

  # Use Fasterer to check various places in your code that could be faster.
  gem 'fasterer', '~> 0.4.0'

  # Memory profiling routines for Ruby 2.1+
  gem 'memory_profiler', '~> 0.9.10'

  # An IRB alternative and runtime developer console
  gem 'pry', '~> 0.11.3'

  # Reek is a tool that examines Ruby classes, modules and methods and reports any code smells it finds.
  gem 'reek', '~> 4.8'

  # Testing framework for Rails 3+
  gem 'rspec-rails', '~> 3.7', '>= 3.7.2'

  # Automatic Ruby code style checking tool
  gem 'rubocop', '~> 0.54.0'

  # Set of matchers methods for TDD
  gem 'shoulda-matchers', '~> 3.1', '>= 3.1.2'

  # Code coverage analysis tool for Ruby
  gem 'simplecov', '~> 0.16.1'

  # Simple tool to test time-dependent code
  gem 'timecop', '~> 0.9.1'

  # Stubbing HTTP requests and setting expectations on HTTP requests
  gem 'webmock', '~> 3.3'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end


# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

# Support for confirm modal
gem 'data-confirm-modal', '~> 1.6', '>= 1.6.2'

# Load environment variables from .env
gem 'dotenv-rails', '~> 2.2', '>= 2.2.1'

# User authentication
gem 'devise', '~> 4.4', '>= 4.4.3'

# Tabler integration
gem 'tabler-rubygem', '~> 0.1.1'

# Easy forms for Rails
gem 'simple_form', '~> 4.0'

gem 'reform-rails', '~> 0.1.7'
